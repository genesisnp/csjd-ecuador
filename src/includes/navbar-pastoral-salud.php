<div class="navbarInterRight pt7 col-xs-3 visible-lg wow fadeInRight" data-wow-delay="0s">
    <div>
        <div class="pd-x-0 pb2">
            <h2 class="titles-navInterR">Pastoral<br>de la Salud</h2>
        </div>
        <ul class="navList-InterRight">
            <li class="navItem-InterRight <?= in_array('dimension-profetica.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="dimension-profetica.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-dimension-profetica iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">dimensión<br>profética</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('dimension-liturgica.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="dimension-liturgica.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-dimension-liturgica iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">dimensión<br>litúrgica</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('dimension-caritativa.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="dimension-caritativa.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-dimension-caritativa iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">dimensión<br>caritativa</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('dimension-hospitalaria.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="dimension-hospitalaria.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-dimension-hospitalaria iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">dimensión<br>hospitalaria</h2>
                </a>
            </li>
        </ul>
    </div>
</div>